import string

import conftest
import random # define the random module



#Used in request for bypass HTTPS(SSL certificate)
verify=False


#Read json data
pla_config_json= conftest.pla_config()
url_json= conftest.URLread()




#Get value from Json data

url=url_json["URI"]["url"]
githuburl=url_json["URI"]["githuburl"]
getRepoDetails=url_json["URI"]["githuburl"]

executionrange=pla_config_json["execution_range"]


#Get resources data
resource_updaterepo=url_json["Resources"]["update_repo"]
resource_GetAllrepoDetails_with_Authentication_User=url_json["Resources"]["GetAllrepoDetails_with_Authentication_User"]
resource_getREpoDetails=url_json["Resources"]["Getrepo_Details"]
resource_Create_Repo_with_Authentication_User=url_json["Resources"]["Create_Repo_with_Authentication_User"]
resource_deleterepo=url_json["Resources"]["delete_repo"]
