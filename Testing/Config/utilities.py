import conftest
from Config.global_declare import executionrange



def getTestCaseList():

    tc = {1: "test_001_Create_Repo_with_Authentication_User",
          2: "test_002_Getrepo_Details",
          3: "test_003_GetAllrepoDetails_with_Authentication_User",
          4: "test_004_UpdateRepo_name",
          5: "test_005_DeleteRepo",
          6:"test_006_Check_specific_repo_present_or_not"
          }
    return tc


def getExecutionRange():
    tc = getTestCaseList()
    tc_list = []
    exerange = executionrange  # value fetch from PLA_config.json for replace through Jenkins/Teamcity
    exerange = conftest.cmdvariable  # when we run in linux no need of this paramter
    data = exerange
    execution_plan = data  # here we need to give value
    execution_range_list = [int(member.strip()) for member in execution_plan.split(',')]
    #print("Execution range is ", execution_range_list)

    if len(execution_range_list) % 2 == 0:
        execution_start_list = execution_range_list[0::2]
        #print("Execution start List", execution_start_list)
        execution_end_list = execution_range_list[1::2]
        #print("Execution End List", execution_end_list)
        j = 0
        for i in execution_start_list:
            st = int(i)
            end = int(execution_end_list[j])
            j += 1
            while st <= end:
                tc_list.append(tc[st])
                st += 1
        #print("TC List is ", tc_list)
    else:
        execution_plan = execution_range_list
        last_execution_start = execution_plan[len(execution_plan) - 1]
        #print("last execution start", last_execution_start)
        #print("Execution range List", execution_range_list)
        execution_start_list = execution_range_list[0::2]
        #print("Execution start List", execution_start_list)
        execution_end_list = execution_range_list[1::2]
        #print("Execution List", execution_end_list)
        j = 0
        for i in execution_start_list:
            st = int(i)
            end = int(execution_end_list[j])
            j += 1
            while st <= end:
                tc_list.append(tc[st])
                st += 1
        while last_execution_start <= len(tc):
            tc_list.append(tc[last_execution_start])
            last_execution_start += 1
    #print("TC List", tc_list)
    execution_list = tc_list
    print("Execution List ", execution_list)
    return execution_list
