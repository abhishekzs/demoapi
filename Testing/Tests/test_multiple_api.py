import json
import random
import string
import time
import pytest
import requests
from Config.utilities import getExecutionRange
import conftest
from Config.global_declare import githuburl, resource_updaterepo,  \
    resource_Create_Repo_with_Authentication_User,  resource_getREpoDetails, \
    resource_GetAllrepoDetails_with_Authentication_User, resource_deleterepo

S = 3
# call random.choices() string module to find the string in Uppercase + numeric data.
ran = ''.join(random.choices(string.ascii_uppercase + string.digits, k = S))
randomdata=str(ran)
#print("The randomly generated string is : " + randomdata) # print the random data

#Execution list
execution_list = getExecutionRange()
#print(execution_list)


@pytest.mark.skipif('test_001_Create_Repo_with_Authentication_User' not in execution_list, reason="Test Case not in execution list")
def test_Create_Repo_with_Authentication_User():

        #start = time.clock()
        bodyData = {"name":randomdata}
        response = requests.post(githuburl + resource_Create_Repo_with_Authentication_User,data=json.dumps(bodyData), headers=conftest.headers,
                                 verify=False, timeout=5)
        asd=response.headers
        asdd=json.dumps(dict(asd))
        json_object = json.loads(asdd)
        #print(json_object["X-GitHub-Request-Id"])
        #print(response.status_code)
        #request_time = time.clock() - start
        #print("Request completed in {0:.0f}ms".format(request_time))
        # store request_time in persistent data store
        assert response.status_code == 201

@pytest.mark.skipif('test_002_Getrepo_Details' not in execution_list, reason="Test Case not in execution list")
def test_Getrepo_Details():
    response = requests.get(githuburl + resource_getREpoDetails + "/RE8", headers=conftest.headers,
                            verify=False, timeout=5)

    jsonload = json.loads(response.text)
    data = json.dumps(jsonload, indent=4)
    #print(data)
    #print(response.status_code)
    assert response.status_code == 200
    #print(jsonload["name"])
    #print(jsonload["owner"]["login"])
    assert jsonload["name"] == "RE8"


@pytest.mark.skipif('test_003_GetAllrepoDetails_with_Authentication_User' not in execution_list,
                    reason="Test Case not in execution list")
def test_GetAllrepoDetails_with_Authentication_User():
    response = requests.get(githuburl + resource_GetAllrepoDetails_with_Authentication_User,
                            headers=conftest.headers,
                            verify=False, timeout=10)
    # print(response.text)
    #print(response.status_code)
    assert response.status_code == 200


@pytest.mark.skipif('test_004_UpdateRepo_name' not in execution_list, reason="Test Case not in execution list")
def test_UpdateRepo_name():
    response = requests.get(githuburl + resource_GetAllrepoDetails_with_Authentication_User,
                                                        headers=conftest.headers,
                                                        verify=False, timeout=10)

    name = json.loads(response.text)
    dta = name[0]["name"]
    bodyData = {"name": dta}
    #print(dta)
    #print(githuburl + resource_updaterepo + "/" + dta)
    response = requests.patch(githuburl + resource_updaterepo + "/"+dta,
                               data=json.dumps(bodyData),headers=conftest.headers,
                              verify=False, timeout=10)
    #print(response.text)
    #print(response.status_code)
    assert response.status_code == 200
@pytest.mark.skipif('test_005_DeleteRepo' not in execution_list, reason="Test Case not in execution list")
def test_DeleteRepo():
    response = requests.get(githuburl + resource_GetAllrepoDetails_with_Authentication_User,
                            headers=conftest.headers,
                            verify=False, timeout=5)
    name = json.loads(response.text)
    dta = name[0]["name"]


    response = requests.delete(githuburl + resource_deleterepo + "/" + dta,
                                   headers=conftest.del_headers,
                                   verify=False, timeout=10)

    #print(response.text)
    #print(response.status_code)
    assert response.status_code == 204


def get_If_expected_repo_is_present(allreponame,data):
    for repo in allreponame:
        #print(repo["name"])
        if repo["name"] == data:
            #print("FOund")
            flag="true"
            break
        else:
            flag = "false"
            #print("Not found")
    return flag


@pytest.mark.skipif('test_006_Check_specific_repo_present_or_not' not in execution_list, reason="Test Case not in execution list")
def test_Check_specific_repo_present_or_not():
    response = requests.get(githuburl + resource_GetAllrepoDetails_with_Authentication_User,
                            headers=conftest.headers,
                            verify=False, timeout=5)
    res = json.loads(response.text)
    expected="LHO"
    data=get_If_expected_repo_is_present(res,expected)
    assert "true"==data




