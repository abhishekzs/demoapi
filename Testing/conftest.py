from  datetime import datetime
import json
import os

import pytest
from requests.structures import CaseInsensitiveDict


#To get current folder path
current_dir = os.path.dirname(os.path.abspath(__file__))

#Create and get data token
headers = CaseInsensitiveDict()
headers["Content-Type"] = "application/json"
headers["Accept"] = "application/json"
headers["Authorization"] = "Bearer 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"


#Delete Token
del_headers = CaseInsensitiveDict()
del_headers["Content-Type"] = "application/json"
del_headers["Accept"] = "application/json"
del_headers["Authorization"] = "Bearer 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"


#Read Json Files


def URLread():
    with open(current_dir+'/JsonData/URL.json','r') as config_file:
        urlinput = json.load(config_file)
        return urlinput

def pla_config():
    with open(current_dir+'/JsonData/PLA_config.json','r') as config_file:
       data = json.load(config_file)
       return data


#Get the value from CMD line
def pytest_addoption(parser):
    parser.addoption("--executionRange", action="store", default="Test", required=False)


cmdvariable = None

def pytest_configure(config):
    global cmdvariable
    cmdvariable = config.getoption('--executionRange')
    return cmdvariable


